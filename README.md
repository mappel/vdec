## UPDATE

This repository is basically obsolte and has been replaced by the **[header parser](https://github.mpi-klsb.mpg.de/mappel/header_parser)**.

## Building on Rechenknecht

Create a build folder wherever you like and execute

`cmake -DCMAKE_BUILD_TYPE=Rechenknecht /path/to/CMakeLists.txt`

You should see a message `Building special Rechenknecht version!` and
also the special link flags (to `/pool/data/...`) should be displayed.

Then just run `make` to make the executable.
