#ifndef VDEC_H_
#define VDEC_H_

#include <string>
#include <cstdint>
extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
}

typedef struct {
  char type;
  int32_t number;
  int32_t length;
  int64_t pos;

  std::string print();
} VideoFrames;

int32_t parse(const std::string &input_video_filename, const std::string &output_csv_filename);

#endif //VDEC_H_
