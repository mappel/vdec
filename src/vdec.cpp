/// A video decoder that (in theory) supports all formats that the underlying libavcodec supports. Input files that are
/// in a container format and include multiple streams are handled in such a way that the "best" (according to some
/// ffmpeg internal metric, see av_find_best_stream() in avformat.h) video stream is chosen and decoded. All other
/// streams are ignored.

#include "vdec.h"
#include <iomanip>
#include <iostream>
#include <fstream>
#include <map>

using std::cerr;
using std::endl;

#define HEX_OUTPUT std::setw(7) << std::setfill('0') << std::uppercase << std::hex

int32_t video_stream_idx = 0;
AVCodecContext *dec_ctx = nullptr;
AVFormatContext *ifmt_ctx = nullptr;
AVFrame *frame = nullptr;
AVPacket *pkt;
int64_t next_pos = 0;
int64_t bytestream_pos = 1;

std::map<int32_t, VideoFrames> frame_map;
std::map<int64_t, int32_t> header_map;

std::string VideoFrames::print() {
  return "type: " + std::string(1, type) + " num: " + std::to_string(number) + " size: " + std::to_string(length)
      + "Byte";
}

void map_to_csv(const std::string &output_csv_filename) {
  std::ofstream csv_file;
  csv_file.open(output_csv_filename, std::ios::out | std::ios::trunc);
  csv_file << "type,num,size" << endl;
  auto header_it = header_map.begin();
  for (auto &frame_it : frame_map) {
    if (header_it->first <= frame_it.first) {
      csv_file << "H,0," << header_it->second << endl;
      header_it++;
    }
    auto current_frame = frame_it.second;
    csv_file << current_frame.type << "," << current_frame.number << "," << current_frame.length << endl;
  }
  csv_file.close();
}

/// Decode the current packet into its frames and print information for each frame.
///
/// @return 0 if everything is OK, a negative value if an error occurred.
int32_t decode_packet() {
  int32_t ret = 0;
  ret = avcodec_send_packet(dec_ctx, pkt);
  if (ret < 0) {
    cerr << "Error sending a packet for decoding" << endl;
    return ret;
  }
  if (pkt != nullptr) {
    cerr << "packet: " << bytestream_pos << " size: " << pkt->size << " pos: " << pkt->pos << " " << HEX_OUTPUT
         << pkt->pos << std::dec << std::endl;
    if (next_pos != pkt->pos) {
      cerr << "MP4 Header from " << HEX_OUTPUT << next_pos << " to " << HEX_OUTPUT << pkt->pos - 1 << std::dec << endl;
      header_map.emplace(bytestream_pos, pkt->pos - next_pos);
    }
    next_pos = pkt->pos + pkt->size;
    bytestream_pos++;
  }
  while (ret >= 0) {
    ret = avcodec_receive_frame(dec_ctx, frame);
    if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
      break;
    } else if (ret < 0) {
      cerr << "Error during decoding" << endl;
      return ret;
    }
    cerr << "frame " << dec_ctx->frame_number << " coded: " << frame->coded_picture_number << " type: "
         << av_get_picture_type_char(frame->pict_type) << " size: " << frame->pkt_size << " pos: " << frame->pkt_pos
         << " " << HEX_OUTPUT << frame->pkt_pos << std::dec << endl;
    frame_map.emplace(std::pair<int32_t, VideoFrames>(frame->coded_picture_number + 1,
                                                      {av_get_picture_type_char(frame->pict_type),
                                                       frame->coded_picture_number + 1, frame->pkt_size,
                                                       frame->pkt_pos}));
  }
  return 0;
}

/// Select the "best" video stream and initialize the corresponding codec context.
///
/// @return 0 if everything is OK, a negative value if an error occurred.
int32_t open_codec_context() {
  AVCodec *dec = nullptr;
  AVStream *istr = nullptr;

  int32_t ret = av_find_best_stream(ifmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, nullptr, 0);
  if (ret < 0) {
    cerr << "Could not find video stream in input file" << endl;
    avformat_close_input(&ifmt_ctx);
    return ret;
  }

  video_stream_idx = ret;
  istr = ifmt_ctx->streams[video_stream_idx];

  dec = avcodec_find_decoder(istr->codecpar->codec_id);
  if (dec == nullptr) {
    cerr << "Failed to find video codec" << endl;
    avformat_close_input(&ifmt_ctx);
    return AVERROR(EINVAL);
  }

  dec_ctx = avcodec_alloc_context3(dec);
  if (dec_ctx == nullptr) {
    cerr << "Failed to allocate the video codec context" << endl;
    avformat_close_input(&ifmt_ctx);
    return AVERROR(ENOMEM);
  }

  ret = avcodec_parameters_to_context(dec_ctx, istr->codecpar);
  if (ret < 0) {
    cerr << "Failed to copy video codec parameters to decoder context" << endl;
    avformat_close_input(&ifmt_ctx);
    avcodec_free_context(&dec_ctx);
    return ret;
  }

  ret = avcodec_open2(dec_ctx, dec, nullptr);
  if (ret < 0) {
    cerr << "Failed to open video codec" << endl;
    avformat_close_input(&ifmt_ctx);
    avcodec_free_context(&dec_ctx);
    return ret;
  }

  return 0;
}

int32_t parse(const std::string &input_video_filename, const std::string &output_csv_filename) {
  int32_t ret = 0;

  cerr << "Using ffmpeg version: " << av_version_info() << endl;

  ret = avformat_open_input(&ifmt_ctx, input_video_filename.c_str(), nullptr, nullptr);
  if (ret < 0) {
    cerr << "Could not open source file " << input_video_filename << endl;
    return ret;
  }

  ret = avformat_find_stream_info(ifmt_ctx, nullptr);
  if (ret < 0) {
    cerr << "Could not find stream information" << endl;
    avformat_close_input(&ifmt_ctx);
    return ret;
  }

  ret = open_codec_context();
  if (ret < 0) {
    return ret;
  }

  cerr << "Video information" << endl;
  cerr << "width: " << dec_ctx->width << " height: " << dec_ctx->height << " codec: "
       << avcodec_get_name(dec_ctx->codec_id) << endl;

  pkt = av_packet_alloc();
  if (pkt == nullptr) {
    cerr << "Could not allocate packet" << endl;
    avformat_close_input(&ifmt_ctx);
    avcodec_free_context(&dec_ctx);
    return AVERROR(ENOMEM);
  }
  av_init_packet(pkt);
  pkt->data = nullptr;
  pkt->size = 0;

  frame = av_frame_alloc();
  if (frame == nullptr) {
    cerr << "Could not allocate frame" << endl;
    avcodec_free_context(&dec_ctx);
    return AVERROR(ENOMEM);
  }

  while (av_read_frame(ifmt_ctx, pkt) >= 0) {
    // Skip packets that do not belong to our desired stream.
    if (pkt->stream_index != video_stream_idx) {
      av_packet_unref(pkt);
      continue;
    }
    ret = decode_packet();
    /*
    // This should possible be removed later, but useful for debugging purposes.
    if (ret < 0) {
      cerr << "Aborting decoding" << endl;
      av_packet_unref(pkt);
      break;
    }*/
    av_packet_unref(pkt);
  }

  // Flush the decoder
  av_packet_free(&pkt);
  decode_packet();

  avcodec_free_context(&dec_ctx);
  avformat_close_input(&ifmt_ctx);
  av_frame_free(&frame);

  map_to_csv(output_csv_filename);

  return 0;
}

int32_t main(int32_t argc, char **argv) {
  if (argc < 3) {
    std::cerr << "usage: " << argv[0] << " <input video file> <output csv file>" << endl;
    return 1;
  }
  return parse(std::string(argv[1]), std::string(argv[2]));
}
